import java.util.*;

public class CodeRandom {
    String karakter = "ABCDEFGHIJKLMNOPQRSTUVWXYZ";
    int panjangKupon = 10;

    Random acak = new Random();
    char[] voucher = new char[panjangKupon];

    public String getRandom() {
        String randomCoupon = "";

        for (int i = 0; i < panjangKupon; i++) {
            voucher[i] = karakter.charAt(acak.nextInt(karakter.length()));
        }

        for (int i = 0; i < voucher.length; i++) {
            randomCoupon += voucher[i];
        }
        return randomCoupon;
    }
}
